ip access-list standard ACL-SNMP-RO-IN
 permit 192.168.1.0 0.0.0.255 log
 deny   any log
ip access-list standard ACL-SNMP-RW-IN
 permit 192.168.1.0 0.0.0.255 log
 deny   any log

snmp ifmib ifindex persist

snmp-server group AUTHGROUP v3 auth 
snmp-server group PRIVGROUP v3 priv 
snmp-server view V3ISO iso included
snmp-server view VIEW-SYSTEM-ONLY system included
snmp-server community <redacted> RO ACL-SNMP-RO-IN
snmp-server location "<location>"
snmp-server contact "<contact info>"