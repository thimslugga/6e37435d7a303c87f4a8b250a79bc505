ip ssh version 2
ip ssh maxstartups 5
ip ssh time-out 60
ip ssh logging events
ip scp server enable

ip ssh rsa keypair-name SSH-KEYPAIR
ip ssh dh min size 4096

ip ssh source-interface Vlan1

ip access-list standard ACL-SSH-IN
 permit 192.168.1.0 0.0.0.255 log
 deny   any log

line vty 0 4
 access-class ACL-SSH-IN in
 exec-timeout 30 0
 logging synchronous
 length 0
 transport preferred none
 transport input ssh
 transport output ssh
 escape-character 3
 
line vty 5 15
 access-class ACL-SSH-IN in
 exec-timeout 30 0
 logging synchronous
 transport preferred none
 transport input ssh
 transport output ssh
 escape-character 3